replaceList = []
# Logs
replaceList.append((r'nlapiLogExecution\([\'"](\w+)[\'"], *[\'"]*([\w ]+)[\'"]*, *[\'"]*([\w ]+)[\'"]*\)', r'log.\1(\2, \3)'))
replaceList.append((r'nlapiLogExecution\([\'"](\w+)[\'"], *[\'"]*([\w ]+)[\'"]*\)', r'log.\1("\2")'))

# Records
replaceList.append((r'nlapiLoadRecord\([\'"]*([\w()]+)[\'"]*, *[\'"]*([\w()]+)[\'"]*\)', r'record.load({type: \1, id: \2})', ('N/record', 'record')))
replaceList.append((r'nlapiGetRecordId\(\)', r'currentRecord.getValue("id")', ('N/currentRecord', 'currentRecord')))

# Mail
replaceList.append((r'getBody\(\)', r'body'))
replaceList.append((r'getSubject\(\)', r'subject'))
replaceList.append((r'nlapiSendEmail\(\s*([\w\-\.,@]*?)\s*,\s*(.*?)\s*,\s*([\w\-\.,@]*?)\s*,\s*(.*?)\s*,\s*(.*?)\s*,\s*(.*?)\s*,\s*(.*?)\s*,\s*(.*?)\s*\)', r'email.send({author: \1, recipients: \2, subject: \3, body: \4, cc: \5, bcc: \6, relatedRecords: \7, attachments: \8})', ('N/email', 'email')))
replaceList.append((r'nlapiSendEmail\(\s*([\w\-\.,@]*?)\s*,\s*(.*?)\s*,\s*([\w\-\.,@]*?)\s*,\s*(.*?)\s*,\s*(.*?)\s*,\s*(.*?)\s*\)', r'email.send({author: \1, recipients: \2, subject: \3, body: \4, cc: \5, bcc: \6})', ('N/email', 'email')))

# Render
replaceList.append((r'nlapiPrintRecord\(\s*\'?(.*?)\'?\s*, \s*(.*?)\s*, \s*(.*?)\s*\)', r'render.\1({entityId: \2, printMode: \3})', ('N/render', 'render')))
replaceList.append((r'nlapiPrintRecord\(\s*\'?(.*?)\'?\s*, \s*(.*?)\s*, \s*(.*?)\s*\)', r'render.\1({entityId: \2, printMode: \3})', ('N/render', 'render')))
replaceList.append((r'render\.TRANSACTION', r'render.transaction'))


# Values and Texts
replaceList.append((r'getFieldValue', r'getValue'))
replaceList.append((r'getValue\(["\']id["\']\)', r'id')) 