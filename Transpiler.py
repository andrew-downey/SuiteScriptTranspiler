import re
import replacements
import jsbeautifier
import logging

class Transpiler:
	def process(self, inputFilename):
		self.inputFilename = inputFilename
		self.data = None

		self.run()
		self.beautify()
		self.save()

	def run(self):
		logging.info('Transpiling ' + self.inputFilename)
		try:
			with open(self.inputFilename, 'r') as file:
				self.data = file.read()
				if(self.data.find('@NApiVersion 2') != -1):
					logging.debug("Refusing to transpile, SS2.0 Header Found in " + self.inputFilename)
					self.data = None
					return
		except:
			logging.debug("Unable to open file " + self.inputFilename)
			return

		requireArray = []

		for regex in replacements.replaceList:
			self.data, num = re.subn(regex[0], regex[1], self.data)
			if(num):
				try:
					requireArray.append(regex[2])
				except:
					pass

		# We can make this "smart" by looking at the function names and making a guess. Should alert the user if the
		# guess is just that, or if we have to default.
		scriptType = 'Suitelet'

		moduleHeader = '/**\n \
			* @NApiVersion 2.x\n \
			* @NScriptType ' + scriptType + '\n \
			*/\n \
			define([\'' + '\', \''.join([module[0] for module in requireArray]) + '\'], function (' + ', '.join([module[1] for module in requireArray]) + ') {\n'

		moduleFooter = '\n});'

		self.data = moduleHeader + self.data + moduleFooter

	def beautify(self):
		if(self.data is not None):
			self.data = jsbeautifier.beautify(self.data)

	def save(self):
		if(self.data is not None):
			outputFile = self.inputFilename[:-3] + '_2.0.js'
			logging.debug("Saving to file " + outputFile)
			with open(outputFile, 'w') as file:
				file.write(self.data)
